# This file is a template, and might need editing before it works on your project.
# This Dockerfile installs a compiled binary into a bare system.
# You must either commit your compiled binary into source control (not recommended)
# or build the binary first as part of a CI/CD pipeline.

FROM alpine:3.14

ENV container docker

USER root

# We'll likely need to add SSL root certificates
RUN apk add bash openjdk11 krb5-server krb5-libs krb5-conf krb5
RUN mkdir -p /usr/local/julie-ops/bin && chmod 755 /usr/local/julie-ops


# Change `app` to whatever your binary is called
Add julie-ops.jar /usr/local/julie-ops/bin
Add julie-ops-cli.sh /usr/local/julie-ops
CMD ["./julie-ops-cli.sh"]

ADD /target/julie-ops.jar julie-ops.jar
ENTRYPOINT ["java","-jar","julie-ops.jar"]
